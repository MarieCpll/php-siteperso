<?php

/* 
vous ajouterez ici les fonctions qui vous sont utiles dans le site,
je vous ai créé la première qui est pour le moment incomplète et qui devra contenir
la logique pour choisir la page à charger
*/

function getContent(){
	if(!isset($_GET['page'])){
		include __DIR__.'/../pages/home.php';
		// include __DIR__.'/../pages/bio.php';
		// include __DIR__.'/../pages/contact.php';
	} else {
		include __DIR__.'/../pages/'.$_GET['page'];

	}
}

function getPart($name){
	include __DIR__ . '/../parts/'. $name . '.php';
}

// créez une fonction getUserData récupérez le contenu du fichier data/user.json grâce à la fonction standard 'file_get_contents' et transformez le json en un tableau PHP avec la fonction standard json_decode puis affichez ces informations

function getUserData(){
	$files= file_get_contents(__DIR__.'/../data/user.json');
	$files= json_decode($files);
	var_dump($files);
}